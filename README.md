﻿Feign Client 


Dependency


<dependency>
   <groupId>org.springframework.cloud</groupId>
   <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>




Configuration


@EnableFeignClients


 Interface
@FeignClient(value= service to be used, url="http://localhost:8900")
public interface StockInterface {


   // the api to be used - replication
   @GetMapping("/{productNumber}")
   Integer getStock(@PathVariable("productNumber") String productNumber);
}






Eureka
Server


Dependency 


<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>


Configuration


@EnableEurekaServer


Application.yml
  

Client
Dependency
<dependency>
   <groupId>org.springframework.cloud</groupId>
   <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>


Configuration
@EnableEurekaClient


Application.yml
  





Interface
@FeignClient("stock-service")
public interface StockInterface {


   @GetMapping("/stock/{productNumber}")
   Integer getStock(@PathVariable("productNumber") String productNumber);
}
Load Balancer
Ribbon is deprecated


Use Load balancer provided by spring


Create two nodes, just by adding new springboot configuration while deployment by changing port nnumber
Dependency
<dependency>
   <groupId>org.springframework.cloud</groupId>
   <artifactId>spring-cloud-starter-loadbalancer</artifactId>
</dependency>


Configuration
@LoadBalancerClient("stock-service")


  















________________

Gateway
Dependency
<dependency>
   <groupId>org.springframework.cloud</groupId>
   <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>




Configuration
@EnableEurekaClient - for using name registered in eureka, else use uri in yml
 
Application.yml
  





________________